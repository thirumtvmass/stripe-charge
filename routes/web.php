<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::pattern('id', '[0-9]+');

Route::group(['prefix' => 'products'], function () {
    Route::get('list', 'App\Http\Controllers\ProductsController::class@list')->name('products.list');
    
});

Route::resource('products', App\Http\Controllers\ProductsController::class);

Route::get('products/list', [ProductsController::class, 'list'])->name('products.list');
Route::get('buy/{id}', [ProductsController::class, 'buy']);
Route::any('products/{id}/purchase', [ProductsController::class, 'purchase'])->name('products.purchase');
