<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use DataTables;
use Auth;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index');
    }

    /**
     * Show the products list.
     *
     * @return \Illuminate\Http\Response
     */

    public function list(Request $request)
    {
        
        if ($request->ajax()) {
            
            $data = Products::latest()->get();
            // print_r($data);exit;
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action','<a href="buy/1" class="badge badge-sm bg-gradient-success btn-sm">Buy Now</a>')
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the product details and card information.
     *
     * @return \Illuminate\Http\Response
     */

    public function buy($id){

        $products = Products::find($id);
        // print_r(Auth::check());exit;
        if(Auth::check())
        $intent = auth()->user()->createSetupIntent();
        else $intent = '';

        return view('products.buy',compact('products', 'intent'));

    }

    public function purchase(Request $request, Products $product)
    {
        $user          = $request->user();
        $paymentMethod = $request->input('payment_method');

        try {
            $user->createOrGetStripeCustomer();
            $user->updateDefaultPaymentMethod($paymentMethod);
            $user->charge($product->price * 100, $paymentMethod);        
        } catch (\Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('message', 'Product purchased successfully!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
